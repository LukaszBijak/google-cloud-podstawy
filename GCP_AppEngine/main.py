from flask import Flask, render_template

app = Flask(__name__,template_folder='templates')

@app.route("/")
def homepage():
    return render_template("index.html", title="Home Page Demo app GCP Course for VideoPoint")

@app.route("/1")
def first():
    return render_template("1.html", title="First Page in Demo")

@app.route("/2")
def second():
    return render_template("2.html", title="Second Page in Demo")

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)
